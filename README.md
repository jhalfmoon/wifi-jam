# wifi-jam

Automated wifi client deauthenticator

This code is a particularly nasty piece of work. It forces deauthentication of authenticated wireless clients using variable intervals. This was an experiment that I used on my own lab-network as a proof of concept to find out how horribly invisible and nasty a wireless network can be DOS'ed. Once operational, the target network will fail at completely random intervals for seemingly no reason whatsoever.

Do not ever use this on anyone. Ever.
